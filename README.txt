// Licence:
This work is licensed under the Creative Commons Attribution 3.0 Unported License. To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/ or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
Use it for whatever, please just attribute the code to abutt1.

// Explanation(s?):

CustomCoroutines are helper classes to help manage coroutines within Unity. They allow you to easily pause/resume/cancel coroutines as well as create custom yield operations. 

// Usage
To use CustomCoroutines in your Unity project just drag the .dll into your project (bin/Release/CoroutinePerformer.dll). Alternatively, you can just drag the scripts into your project. If you want to build the .sln, you will probably need to link the UnityEngine dll's.

// Examples:

To create a coroutine that Unity pumps automatically, simply call:

    :::c
        void Foo() {
            CustomCoroutinePerformer.PerformCoroutine(ExampleEnumerator());
        }

        IEnumerator ExampleEnumerator( ) {
            yield return false;
        }

If you want to pause/cancel the coroutine just keep a reference to it:

    :::c
        void Foo() {
            ICustomCoroutine myCoroutine = CustomCoroutinePerformer.PerformCoroutine(ExampleEnumerator());
            myCoroutine.Pause();
        }

If you want to manually create and pump a coroutine

    :::c
        void Foo() {
            ICustomCoroutine myCoroutine = new CustomCoroutine(ExampleEnumerator());
            myCoroutine.Pump();
        }

To create a custom yield operation, simply implement the ICustomYieldInstruction interface (for some examples, look at YieldInstructions.cs).