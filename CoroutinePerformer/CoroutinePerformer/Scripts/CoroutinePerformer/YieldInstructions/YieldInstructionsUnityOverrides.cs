using UnityEngine;
using System.Collections;
using System.Reflection;

namespace abutt1.CustomCoroutines
{

	#region UnityMatchers
	public class CustomYieldWaitForSeconds : ICustomYieldInstruction
	{
		public  float                      GetProgress ()       { return m_duration > 0 ? Mathf.Clamp01( (Time.realtimeSinceStartup - m_startTime)/ m_duration ) : 1; }
		public  bool                       IsDone ()            { return (GetProgress() >= 1);                                                                        }
		
		private float   m_duration;
		private float   m_startTime;
		
		public CustomYieldWaitForSeconds(float seconds)
		{
			m_duration  = seconds;
			m_startTime = Time.realtimeSinceStartup;
		}
		
		public static explicit operator CustomYieldWaitForSeconds(WaitForSeconds other)
		{
			if(other != null)
			{
				float tmpSeconds = 0;
				
				FieldInfo tmpFI = other.GetType().GetField("m_Seconds", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
				
				if(tmpFI != null)
				{
					 tmpSeconds = (float)tmpFI.GetValue(other);
				}
				
				return new CustomYieldWaitForSeconds(tmpSeconds);
			}
			else
			{
				return null;
			}
		}
		
	}
	
	public class CustomYieldWWW : ICustomYieldInstruction
	{
		public  float                       GetProgress ()      { return www != null ? www.progress : 0;      }
		public  bool                        IsDone ()           { return www != null ? (www.isDone || !string.IsNullOrEmpty(www.error)) : true;          }
		
		private WWW www;
		
		public CustomYieldWWW(WWW withWWW)
		{
			www = withWWW;
		}
		
		public static explicit operator CustomYieldWWW(WWW fromWWW)
		{
			return new CustomYieldWWW(fromWWW);
		}
		
		
	}

	public class CustomYieldAsyncOperation : ICustomYieldInstruction
	{
		public  float                      GetProgress ()       { return m_operation != null ? m_operation.progress : 0;                                              }
		public  bool                       IsDone ()            { return m_operation != null ? m_operation.isDone   : (GetProgress() >= 1);                           }
		
		private AsyncOperation   m_operation;
		
		public CustomYieldAsyncOperation(AsyncOperation withOperation)
		{
			m_operation = withOperation;
		}
		
		public static explicit operator CustomYieldAsyncOperation(AsyncOperation other)
		{
			return new CustomYieldAsyncOperation(other);
		}
		
	}
	#endregion

}
