using UnityEngine;
using System.Collections;

namespace abutt1.CustomCoroutines
{
	public class CustomCoroutinePerformer : MonoBehaviour
	{
		#region Interface
		
		public static ICustomCoroutine PerformCoroutine(IEnumerator routine)
		{
			return PerformCoroutine(routine, null, null);
		}
		
		public static ICustomCoroutine PerformCoroutine(IEnumerator routine, MessageDelegate exceptionCallback )
		{
			return PerformCoroutine(routine, null, exceptionCallback);
		}		

		public static ICustomCoroutine PerformCoroutine(IEnumerator routine, CoroutineDelegate finishedCallback )
		{
			return PerformCoroutine(routine, finishedCallback, null);
		}		

		public static ICustomCoroutine PerformCoroutine(IEnumerator routine, CoroutineDelegate finishedCallback, MessageDelegate exceptionCallback )
		{
			return PerformCoroutineLogic(routine, finishedCallback, exceptionCallback);
		}		
		
		#endregion
		
		private ICustomCoroutine currentCoroutine;
		
		#region MonoEvents
		
		private void OnEnable()
		{
			if(currentCoroutine != null)
			{
				// we've been disabled, so resume the coroutine(?)
				if(!currentCoroutine.Finished || !currentCoroutine.Cancelled)
				{
					currentCoroutine.Resume();
				}
				else
				{
					DestroyPerformer();
				}
			}			
		}
		
		private void OnDisable()
		{
			if(currentCoroutine != null)
			{
				// we've been disabled, so pause the coroutine(?)
				// we can only resume if the gameobject is active. if it isn't, the coroutine has been cancelled
				
				if(this.gameObject.activeInHierarchy)
				{
					currentCoroutine.Pause();
				}
				else
				{
					// wrap up
					DestroyPerformer();
				}
			}
		}
		
		private void OnDestroy()
		{
			if(currentCoroutine != null)
			{
				CancelCurrentCoroutine();
			}
		}
		
		#endregion
		
		#region InternalLogic
		
		private static GameObject performerGO;
		
		private static ICustomCoroutine PerformCoroutineLogic(IEnumerator routine, CoroutineDelegate finishedCallback, MessageDelegate exceptionCallback)
		{
			// don't bother if no data
			if(routine == null)
			{
				return null;
			}
			
			// create our static performer if we need to
			if(performerGO == null)
			{
				performerGO = new GameObject("CustomCoroutinePerformerGO");
			}
			
			// create a new performer
			CustomCoroutinePerformer tmpPerformer = performerGO.AddComponent<CustomCoroutinePerformer>();
			
			// create our wrapper coroutine
			CustomCoroutine wrapperCoroutine = new CustomCoroutine(routine);
			
			// register for our events
			wrapperCoroutine.FinishedEvent  += tmpPerformer.OnCoroutineFinished;
			wrapperCoroutine.ExceptionEvent += tmpPerformer.OnCoroutineError;
			if(finishedCallback != null)
			{
				wrapperCoroutine.FinishedEvent  += finishedCallback;
			}
			if(exceptionCallback != null)
			{
				wrapperCoroutine.ExceptionEvent += exceptionCallback;
			}
			
			// start the coroutine up
			tmpPerformer.StartCoroutine(wrapperCoroutine);
			
			
			
			tmpPerformer.currentCoroutine = wrapperCoroutine;
			
			return tmpPerformer.currentCoroutine;
		}
		
		private void OnCoroutineError(string message)
		{
			//Debug.LogWarning("Exception thrown in a coroutine:\n" + message);
			
			// destroy ourselves if error (as coroutine is obviously finished)
			DestroyPerformer();
		}
		
		private void OnCoroutineFinished(ICustomCoroutine coroutine)
		{
			// destroy ourselves (as coroutine is obviously finished and we no longer need to perform ourselves)
			DestroyPerformer();
		}
		
		private void DestroyPerformer()
		{
			// cancel the coroutine if required
			if(currentCoroutine != null)
			{
				CancelCurrentCoroutine();

				Destroy(this);
			}
			
		}

		private void CancelCurrentCoroutine()
		{
			if(currentCoroutine != null)
			{
				// unsubscribe from any events
				currentCoroutine.FinishedEvent  -= OnCoroutineFinished;
				currentCoroutine.ExceptionEvent -= OnCoroutineError;
				
				currentCoroutine.Cancel();
				
				currentCoroutine = null;
			}
		}
		
		#endregion
	}
	
	
}
