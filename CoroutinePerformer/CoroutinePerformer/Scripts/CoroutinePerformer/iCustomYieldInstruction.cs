
namespace abutt1.CustomCoroutines
{

	public interface ICustomYieldInstruction
	{
		bool                      IsDone();                         // if the operation has been completed
		
		float                     GetProgress();                    // get current progress. just for the user to poll if they want to
	}

}
