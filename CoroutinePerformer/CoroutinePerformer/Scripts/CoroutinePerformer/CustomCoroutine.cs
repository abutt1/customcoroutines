using UnityEngine;
using System.Collections;

namespace abutt1.CustomCoroutines
{
	public  delegate    void    CoroutineDelegate(ICustomCoroutine coroutine);
	public  delegate    void    MessageDelegate(string message);
		
	public interface ICustomCoroutine
	{
		bool    Finished    { get; }
		bool    Cancelled   { get; }
		void    Cancel();
		
		bool    Running     { get; }
		void    Pause();
		void    Resume();

		bool    Pump();

		event   MessageDelegate     ExceptionEvent;
		event   CoroutineDelegate   FinishedEvent;
	}
		
	public class CustomCoroutine : ICustomCoroutine, IEnumerator, ICustomYieldInstruction
	{
		private IEnumerator             currentOp;

		private MessageDelegate         exceptionDelegate;
		private CoroutineDelegate       finishedDelegate;

		// We define our add/remove handlers in order for dll packaged code to run on aot builds
		public  event MessageDelegate   ExceptionEvent
		{
			add     { exceptionDelegate += value; }
			remove  { exceptionDelegate -= value; }
		}
		public  event CoroutineDelegate FinishedEvent
		{
			add     { finishedDelegate  += value; }
			remove  { finishedDelegate  -= value; }
		}
				
		private bool                    paused;
		private bool                    cancelled;
		
		#region Constructors
		
		public CustomCoroutine(IEnumerator routine)                                     : this(routine, null, null) {}
		public CustomCoroutine(IEnumerator routine, CoroutineDelegate finishedCallback) : this(routine, finishedCallback, null) {}
		public CustomCoroutine(IEnumerator routine, MessageDelegate exceptionCallback)  : this(routine, null, exceptionCallback) {}

		public CustomCoroutine(IEnumerator routine, CoroutineDelegate finishedCallback, MessageDelegate exceptionCallback)
		{
			currentOp   = CoroutineWrapperLogic( routine );

			if(finishedCallback != null)    { FinishedEvent  += finishedCallback;   }
			if(exceptionCallback != null)   { ExceptionEvent += exceptionCallback;  }
		}
		
		~CustomCoroutine()
		{
			if(!Finished)
			{
				// throw an exception
				RaiseExceptionEvent ("Coroutine object destroyed before completion");
			}
		}
		
		#endregion
		
		
		#region InternalLogic

		IEnumerator CoroutineWrapperLogic(IEnumerator toWrap)
		{
			while(currentOp != null)
			{
				// continue if paused
				if(paused)
				{
					yield return false;
					continue;
				}
				
				
				try
				{
					if( !toWrap.MoveNext() )
					{
						break;
					}
				} catch (System.Exception ex) {
					currentOp = null;
					RaiseExceptionEvent(ex.Message);
					yield break;
				}
				
				// work out if the last returned obj is something we need to handle or not
				// if it isn't, we can just return it
				// if it is something we need to handle, somehow take care of the logic ourselves
				
				object tmpCurrent = toWrap.Current;
				ICustomYieldInstruction currentYieldInstruction = GetCustomYieldInstructionFromObject(tmpCurrent);
				
				if(currentYieldInstruction != null)
				{
					// we need to handle it
					
					while(currentYieldInstruction != null)
					{
						// we need to check ourselves whether it's good to go or not
						if(currentYieldInstruction.IsDone())
						{
							// we've finished
							currentYieldInstruction = null;
						}
						else
						{
							// we need to wait
							yield return false;
						}
					}
				}
				else
				{
					// unity can handle it
					yield return tmpCurrent;
				}
				
			}

			// we have finished
			currentOp = null;
			
			RaiseFinishedEvent(this);
			
		}
		
		ICustomYieldInstruction GetCustomYieldInstructionFromObject(object toCheck)
		{
			if(toCheck == null)                                             { return null; }
			if(toCheck is ICustomYieldInstruction)                          { return (ICustomYieldInstruction)toCheck; }
			if((toCheck.GetType().GetInterface("IEnumerator") != null) )    { return new CustomCoroutine((IEnumerator)toCheck); }
			
			return null;
		}
	
		void RaiseExceptionEvent(string message)
		{
			if(exceptionDelegate != null)       { exceptionDelegate(message); }
		}

		void RaiseFinishedEvent(ICustomCoroutine coroutine)
		{
			if(finishedDelegate != null)        { finishedDelegate(coroutine); }
		}
		
		#endregion
		
		#region ICustomCoroutine
		
		public bool Finished
		{
			get 
			{
				return (currentOp == null);
			}
		}
		public bool Cancelled 
		{
			get 
			{
				return cancelled;
			}
		}
		public void Cancel ()
		{
			cancelled = true;
			if(currentOp != null)
			{
				currentOp = null;
				RaiseExceptionEvent("CustomCoroutine cancelled before completion.");
			}
		}
		public bool Running
		{
			get
			{
				return (!Finished && !paused);
			}
		}
		public void Resume ()
		{
			paused = false;
		}
		public void Pause ()
		{
			paused = true;
		}
		public bool Pump ()
		{
			return ((IEnumerator)this).MoveNext();
		}
		#endregion
		
		// keeping this private so the user doesn't have to worry about it
		#region IEnumerator
		bool    IEnumerator.MoveNext ()
		{
			if(currentOp != null)
			{
				return currentOp.MoveNext();
			}
			else
			{
				// throw an exception?
				if(!Cancelled)
				{
					RaiseExceptionEvent("MoveNext() called but internal operation is null -> likely cancelled.");
				}
				else
				{
					// potentially let the user know?
				}
				return false;
			}
		}
		object  IEnumerator.Current {
			get 
			{
				if(currentOp != null)
				{
					return currentOp.Current;
				}
				else
				{
					// throw an exception?
					return null;
				}
			}
		}
		void    IEnumerator.Reset ()
		{
			
		}		
		#endregion
		
		// keeping this private so the user doesn't have to worry about it
		#region ICustomYieldInstructions
		float ICustomYieldInstruction.GetProgress ()    { return ((ICustomYieldInstruction)this).IsDone() ? 1 : 0;    }
		bool  ICustomYieldInstruction.IsDone ()         { return this.Finished; }
		#endregion
	}
	
}
